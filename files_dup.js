var files_dup =
[
    [ "Closed_Loop.py", "Closed__Loop_8py.html", [
      [ "Lupdate", "classClosed__Loop_1_1Lupdate.html", "classClosed__Loop_1_1Lupdate" ]
    ] ],
    [ "Controller.py", "Controller_8py.html", [
      [ "controller", "classController_1_1controller.html", "classController_1_1controller" ]
    ] ],
    [ "Controllerlab7.py", "Controllerlab7_8py.html", [
      [ "controller", "classControllerlab7_1_1controller.html", "classControllerlab7_1_1controller" ]
    ] ],
    [ "ElevatorHW.py", "ElevatorHW_8py.html", [
      [ "TaskElevator", "classElevatorHW_1_1TaskElevator.html", "classElevatorHW_1_1TaskElevator" ],
      [ "Button", "classElevatorHW_1_1Button.html", "classElevatorHW_1_1Button" ],
      [ "MotorDriver", "classElevatorHW_1_1MotorDriver.html", "classElevatorHW_1_1MotorDriver" ]
    ] ],
    [ "encoder_shares.py", "encoder__shares_8py.html", "encoder__shares_8py" ],
    [ "encodertasks.py", "encodertasks_8py.html", [
      [ "encoder_driver", "classencodertasks_1_1encoder__driver.html", "classencodertasks_1_1encoder__driver" ],
      [ "encoder_task", "classencodertasks_1_1encoder__task.html", "classencodertasks_1_1encoder__task" ]
    ] ],
    [ "ETmain.py", "ETmain_8py.html", "ETmain_8py" ],
    [ "ETuser.py", "ETuser_8py.html", [
      [ "UI_task", "classETuser_1_1UI__task.html", "classETuser_1_1UI__task" ]
    ] ],
    [ "Fibonacci_Sequence.py", "Fibonacci__Sequence_8py.html", "Fibonacci__Sequence_8py" ],
    [ "LED_Main.py", "LED__Main_8py.html", "LED__Main_8py" ],
    [ "motordriver.py", "motordriver_8py.html", [
      [ "MotorDriver", "classmotordriver_1_1MotorDriver.html", "classmotordriver_1_1MotorDriver" ]
    ] ],
    [ "NucleoLED_02.py", "NucleoLED__02_8py.html", "NucleoLED__02_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "UI_DataGen.py", "UI__DataGen_8py.html", [
      [ "UI_DataGenTask", "classUI__DataGen_1_1UI__DataGenTask.html", "classUI__DataGen_1_1UI__DataGenTask" ]
    ] ],
    [ "UI_Front.py", "UI__Front_8py.html", [
      [ "UI_Task", "classUI__Front_1_1UI__Task.html", "classUI__Front_1_1UI__Task" ]
    ] ],
    [ "usr_front.py", "usr__front_8py.html", [
      [ "UI_Task", "classusr__front_1_1UI__Task.html", "classusr__front_1_1UI__Task" ]
    ] ],
    [ "usr_front_main.py", "usr__front__main_8py.html", "usr__front__main_8py" ],
    [ "usr_front_mainlab7.py", "usr__front__mainlab7_8py.html", "usr__front__mainlab7_8py" ],
    [ "usr_frontlab7.py", "usr__frontlab7_8py.html", [
      [ "UI_Task", "classusr__frontlab7_1_1UI__Task.html", "classusr__frontlab7_1_1UI__Task" ]
    ] ],
    [ "usr_task.py", "usr__task_8py.html", [
      [ "taskusr", "classusr__task_1_1taskusr.html", "classusr__task_1_1taskusr" ]
    ] ],
    [ "usr_tasklab7.py", "usr__tasklab7_8py.html", [
      [ "taskusr", "classusr__tasklab7_1_1taskusr.html", "classusr__tasklab7_1_1taskusr" ]
    ] ]
];
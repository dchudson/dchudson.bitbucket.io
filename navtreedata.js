/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "David's Workbook", "index.html", [
    [ "Portfolio Details", "index.html#sec_port", null ],
    [ "Lab0x00", "page_starting.html", [
      [ "Python files for Lab0x00", "page_starting.html#StartFiles", null ],
      [ "Documentation", "page_starting.html#page_starting_doc", null ]
    ] ],
    [ "Fibonacci", "page_fibonacci.html", [
      [ "Python files for Fibonacci_Sequence", "page_fibonacci.html#FFiles", null ],
      [ "Documentation", "page_fibonacci.html#page_fib_doc", null ]
    ] ],
    [ "ElevatorFSM", "page_elevator.html", [
      [ "Python files for ElevatorFSM", "page_elevator.html#EFiles", null ],
      [ "Documentation", "page_elevator.html#page_elevator_doc", null ]
    ] ],
    [ "NucleoLED", "page_nucleomain.html", [
      [ "Python files for the Nucleo virtual and real LED", "page_nucleomain.html#LEDFiles", null ],
      [ "Documentation", "page_nucleomain.html#page_nucleomain_doc", null ]
    ] ],
    [ "NucleoWithEncoder", "page_EncoderMain.html", [
      [ "Python files for encoder on motor", "page_EncoderMain.html#EncoderFiles", null ],
      [ "Documentation", "page_EncoderMain.html#page_EncoderMain_doc", null ]
    ] ],
    [ "UI Data Gen and UI Front", "page_UI_DataGen_and_UIFront.html", [
      [ "Python files for collecting data from encoder on motor using the Nucleo", "page_UI_DataGen_and_UIFront.html#UIFiles", null ],
      [ "Documentation", "page_UI_DataGen_and_UIFront.html#page_UI_DataGen_and_UIFront_doc", null ]
    ] ],
    [ "bttask", "page_bluetooth.html", [
      [ "Python files for using bluetooth along with the Nucleo", "page_bluetooth.html#btFiles", null ],
      [ "Documentation", "page_bluetooth.html#page_bluetooth_doc", null ]
    ] ],
    [ "Lab 6 Encoder Motor System", "page_Lab_6_encoder_motor_system.html", [
      [ "Python files for running the motor and encoder system", "page_Lab_6_encoder_motor_system.html#Lab6Files", null ],
      [ "Documentation", "page_Lab_6_encoder_motor_system.html#page_Lab_6_encoder_motor_system_doc", null ]
    ] ],
    [ "Lab 7 Encoder Motor Trace System", "page_Lab_7_encoder_motor_system.html", [
      [ "Python files for running the motor and encoder system.", "page_Lab_7_encoder_motor_system.html#Lab7Files", null ],
      [ "Documentation", "page_Lab_7_encoder_motor_system.html#page_Lab_7_encoder_motor_system_doc", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"classencodertasks_1_1encoder__driver.html#ae06c776c56730d7a085b06baec45dca3"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
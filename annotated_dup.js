var annotated_dup =
[
    [ "bttask", null, [
      [ "btdriver", "classbttask_1_1btdriver.html", "classbttask_1_1btdriver" ]
    ] ],
    [ "Closed_Loop", null, [
      [ "Lupdate", "classClosed__Loop_1_1Lupdate.html", "classClosed__Loop_1_1Lupdate" ]
    ] ],
    [ "Controller", null, [
      [ "controller", "classController_1_1controller.html", "classController_1_1controller" ]
    ] ],
    [ "Controllerlab7", null, [
      [ "controller", "classControllerlab7_1_1controller.html", "classControllerlab7_1_1controller" ]
    ] ],
    [ "ElevatorHW", null, [
      [ "Button", "classElevatorHW_1_1Button.html", "classElevatorHW_1_1Button" ],
      [ "MotorDriver", "classElevatorHW_1_1MotorDriver.html", "classElevatorHW_1_1MotorDriver" ],
      [ "TaskElevator", "classElevatorHW_1_1TaskElevator.html", "classElevatorHW_1_1TaskElevator" ]
    ] ],
    [ "encodertasks", null, [
      [ "encoder_driver", "classencodertasks_1_1encoder__driver.html", "classencodertasks_1_1encoder__driver" ],
      [ "encoder_task", "classencodertasks_1_1encoder__task.html", "classencodertasks_1_1encoder__task" ]
    ] ],
    [ "ETuser", null, [
      [ "UI_task", "classETuser_1_1UI__task.html", "classETuser_1_1UI__task" ]
    ] ],
    [ "motordriver", null, [
      [ "MotorDriver", "classmotordriver_1_1MotorDriver.html", "classmotordriver_1_1MotorDriver" ]
    ] ],
    [ "NucleoLED_02", null, [
      [ "TaskrLED", "classNucleoLED__02_1_1TaskrLED.html", "classNucleoLED__02_1_1TaskrLED" ],
      [ "VirtualLED", "classNucleoLED__02_1_1VirtualLED.html", "classNucleoLED__02_1_1VirtualLED" ],
      [ "vLED_switch", "classNucleoLED__02_1_1vLED__switch.html", "classNucleoLED__02_1_1vLED__switch" ]
    ] ],
    [ "UI_DataGen", null, [
      [ "UI_DataGenTask", "classUI__DataGen_1_1UI__DataGenTask.html", "classUI__DataGen_1_1UI__DataGenTask" ]
    ] ],
    [ "UI_Front", null, [
      [ "UI_Task", "classUI__Front_1_1UI__Task.html", "classUI__Front_1_1UI__Task" ]
    ] ],
    [ "usr_front", null, [
      [ "UI_Task", "classusr__front_1_1UI__Task.html", "classusr__front_1_1UI__Task" ]
    ] ],
    [ "usr_frontlab7", null, [
      [ "UI_Task", "classusr__frontlab7_1_1UI__Task.html", "classusr__frontlab7_1_1UI__Task" ]
    ] ],
    [ "usr_task", null, [
      [ "taskusr", "classusr__task_1_1taskusr.html", "classusr__task_1_1taskusr" ]
    ] ],
    [ "usr_tasklab7", null, [
      [ "taskusr", "classusr__tasklab7_1_1taskusr.html", "classusr__tasklab7_1_1taskusr" ]
    ] ]
];